// entrypoint for develop in watch mode only
import 'core-js/es/promise'
const faker = require('faker')

import * as L from 'leaflet'
import 'leaflet/dist/leaflet.css'

import {SimpleCluster} from "./SimpleCluster"

const geoJson = require('../examples/markers.1000.geojson.json')
/*
geoJson.features.forEach(f => {
  f.properties.tempAir = faker.random.number({min: 9, max: 40})
  f.properties.tempWater = faker.random.number({min: 9, max: 40})
})
console.log(JSON.stringify(geoJson))
*/

// создание карты
const map = L.map(document.getElementById('map'), {
  zoomControl: false
}).setView([47, 2], 7)

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map)

// инициализация кластеров
const cluster = new SimpleCluster().addTo(map)

// подгрузка новых данных
cluster.cleanPoints().loadGeoJsonData(geoJson)

document.getElementById('randomPoints').addEventListener('click', () => {
  cluster.cleanPoints().loadGeoJsonData(
    {
      type: "FeatureCollection",
      features: geoJson.features.slice(
        faker.random.number({min: 0, max: 500}),
        faker.random.number({min: 501, max: 900})
      )
    }
  )
})

// обработка событий
cluster.on('clusterclick', ({layer}) => alert(`clusterclick, ${cluster.getCountAndTypeInCluster(layer)}`))
cluster.on('click', ({layer}) => alert(`click, ${JSON.stringify(layer.feature)}`))
