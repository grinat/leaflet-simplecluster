import 'core-js/es/object'

import './cluster.scss'

import * as L from 'leaflet'
import 'leaflet.markercluster'
import bbox from '@turf/bbox'

const Types = {
  season: 'is-season',
  nonSeason: 'is-non-season'
}

export const SimpleCluster = L.SimpleCluster = L.FeatureGroup.extend({
  Types,
  options: {
    spiderfyDistanceMultiplier: 3,
    showCoverageOnHover: false,
    clusterIconCreateFunction: null,
    pointIconCreateFunction: null,
    infoCreateFunction: null,
    legendCreateFunction: null,
    customZoomControl: true,
    centerMapOnNewData: true,
    removeAnimateDelay: 300,
    getClusterType (layers) {
      // сластер сезонный если хотя бы один маркер что в него входит сезон
      const isSeason = layers.some(l => l.feature.properties.isSeason === true)

      return isSeason ? Types.season : Types.nonSeason
    },
    getMarkerType (feature) {
      return feature.properties.isSeason ? Types.season : Types.nonSeason
    },
    getCityName (feature) {
      return feature.properties.cityName
    }
  },
  _markersLayer: null,
  _defaultClusterIconCreateFunction (cluster) {
    const [count, type] = this.getCountAndTypeInCluster(cluster)

    return L.divIcon({
      // если меняются размеры маркера
      // меняйте Point(ширина, высота) иначе неверно будет позиционироватся
      iconSize: new L.Point(48, 48),
      className: 'simple-cluster',
      html: `
        <div class="simple-cluster__cluster simple-cluster__cluster--${type}">
          <div class="simple-cluster__cluster__counter">
             ${count}
          </div>
        </div>
        `
    })
  },
  _defaultPointIconCreateFunction (feature) {
    const text = this.options.getCityName(feature) || ' - '

    return L.divIcon({
      iconSize: new L.Point(28 + text.length * 10 + 5, 31),
      className: 'simple-cluster',
      html: `
        <div class="simple-cluster__point simple-cluster__point--${this.options.getMarkerType(feature)}">
          <div class="simple-cluster__point__baloon"></div>
          <div class="simple-cluster__point__title">
             ${text}
          </div>
        </div>
        `
    })
  },
  _defaultInfoCreateFunction (feature) {
    return `<div class="simple-cluster__info">
        <div class="simple-cluster__info__air-icon"></div>
        <div class="simple-cluster__info__air">${feature.properties.tempAir || ' - '}&deg;</div>
        <div class="simple-cluster__info__water-icon"></div>
        <div class="simple-cluster__info__water">${feature.properties.tempWater || ' - '}&deg;</div>
    </div>`
  },
  _defaultCreateLegendFunction () {
    return `<div class="simple-cluster__legend">
       <div class="simple-cluster__dot-black"></div>
       <div class="simple-cluster__text-black">сезон</div>
       <div class="simple-cluster__dot-gray"></div>
       <div class="simple-cluster__text-gray">не сезон</div>
    </div>`
  },
  getCountAndTypeInCluster (cluster) {
    const clusters = cluster.getAllChildMarkers()

    return [
      clusters.length,
      this.options.getClusterType(clusters)
    ]
  },
  cleanPoints() {
    if (this._markersLayer) {
      this._markersLayer.clearLayers()
      this._markersLayer = null
    }

    this._closeInfo()

    return this
  },
  _closeInfo () {
    if (!this._info) {
      return
    }
    this._info.innerHTML = ''
    this._lastOpenedInfo = null
  },
  _openInfo (layer) {
    if (!this._info) {
      return
    }

    this._info.innerHTML = this.options.infoCreateFunction(layer.feature)
    this._lastOpenedInfo = layer
  },
  loadGeoJsonData(geoJson) {
    if (!(geoJson && geoJson.type === 'FeatureCollection')) {
      throw new Error(`Incorect json, need: {
        type: "FeatureCollection",
        features: [...]
      }`)
    }

    const iconCreateFunction = (this.options.clusterIconCreateFunction || this._defaultClusterIconCreateFunction).bind(this)

    const pointIconCreateFunction = (this.options.pointIconCreateFunction || this._defaultPointIconCreateFunction).bind(this)

    this._markersLayer = L.markerClusterGroup({
      ...this.options,
      iconCreateFunction
    })

    const geoJsonLayers = L.geoJSON(geoJson, {
      pointToLayer (feature, latlng) {
        return  L.marker(latlng, {
          icon: pointIconCreateFunction(feature)
        })
      }
    }).getLayers()

    if (this.options.centerMapOnNewData) {
      this._centerMap(geoJson)
    }

    this._markersLayer.addLayers(geoJsonLayers).addTo(this._map)

    this._markersLayer.on('click', (...args) => this.fire('click', ...args))
    this._markersLayer.on('clusterclick', (...args) => this.fire('clusterclick', ...args))

    this._markersLayer.on('mouseover', ({layer} = {}) => {
      this._openInfo(layer)
    })
    this._markersLayer.on('mouseout', ({layer} = {}) => {
      this._closeInfo(layer)
    })
  },
  _centerMap (geoJson) {
    if (!geoJson.features.length) {
      this._map.fitWorld()
      return
    }

    const [longitude1 = 0, latitude1 = 0, longitude2 = 0, latitude2 = 0] = bbox(geoJson)

    this._map.fitBounds([
      [latitude1, longitude1],
      [latitude2, longitude2]
    ], {
      maxZoom: 13,
      // оступы от легенды и кнопок зума
      paddingTopLeft: L.point({x: 70, y: 25}),
      paddingBottomRight: L.point({x: 130, y: 25})
    })
  },
  /**
   * @override
   */
  initialize (options = {}) {
    for (const opt in options) {
      if (this.options.hasOwnProperty(opt)) {
        if (this.options[opt] instanceof Object) {
          this.options[opt] = Object.assign(this.options[opt], options[opt])
        } else {
          this.options[opt] = options[opt]
        }
      }
    }

    this.options.infoCreateFunction = (this.options.infoCreateFunction || this._defaultInfoCreateFunction).bind(this)
    this.options.legendCreateFunction = (this.options.legendCreateFunction || this._defaultCreateLegendFunction).bind(this)
  },
  /**
   * @override
   */
  onAdd (map) {
    this._map = map

    const cont = map.getContainer()

    this._info = L.DomUtil.create('div', 'simple-cluster', cont)
    this._legend = L.DomUtil.create('div', 'simple-cluster', cont)

    this._legend.innerHTML = this.options.legendCreateFunction()

    if (this.options.customZoomControl) {
      this._customZoom = L.control.zoom({
        position:'bottomright',
        zoomInText: '<div></div>',
        zoomOutText: '<div></div>'
      }).addTo(map)

      L.DomUtil.addClass(
        this._customZoom.getContainer(),
        'simple-cluster'
      )
    }

    this._map.on('zoomend', this._zoomEnd, this)
    this._map.on('moveend', this._moveEnd, this)
  },
  onRemove (map) {
    map.off('zoomend', this._zoomEnd, this)
    map.off('moveend', this._moveEnd, this)

    this.cleanPoints()
  },
  _zoomEnd () {
    this._checkInfo()
  },
  _moveEnd () {
    this._checkInfo()
  },
  _checkInfo () {
    if (!this._lastOpenedInfo) {
      return
    }

    if (!this._markersLayer) {
      return
    }

    const removeAnimateDelay = this.options.removeAnimateDelay
    setTimeout(() => {
      const notExist = !this._lastOpenedInfo
      const exitAndNotIcon = this._lastOpenedInfo && !this._lastOpenedInfo._icon

      if (notExist || exitAndNotIcon) {
        this._closeInfo()
      }
    }, removeAnimateDelay + 1)
  }
})
